package services;

import dataBase.Client;
import dataBase.Place;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.MPlace;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class PlaceListService {

	/**
	 * Cr�e une variable list de type List
	 */
	private List<Place> list;
	/**
	 * Cr�e une variable listM de type List qui correspond aux emplacement r�serv�s
	 */
	private List<MPlace> listM;

	/**
	 * Constructeur de la classe PlaceListService qui permet d'initialiser la variables
	 */
	public PlaceListService() {

		this.list = new ArrayList<>();
		this.listM = new ArrayList<>();
	}

	/**
	 * La m�thode getList() permet de retourner la liste des emplacements
	 * @return la liste des emplacements
	 */
	public List<Place> getList() {
		return list;
	}
 
	/**
	 * La m�thode getListM permet de retourner la liste des emplacements r�serv�s
	 * @return listM
	 */
	public List<MPlace> getListM() {
		return listM;
	}

	/**
	 * La m�thode get() permet de retourner un emplacement
	 * @param id
	 * @return un emplacement
	 */
	public Place get(String id) {
		for(Place item : list) {
			if (item.getId().equals(id)) return item;
		}
		return null;
	}

	/**
	 * La m�thode getM() permet de retourner un emplacement (qui est visible sur l'interface graphique)
	 * @param id
	 * @return un emplacement (qui est visible sur l'interface graphique)
	 */
	public MPlace getM(String id) {
		for(MPlace item : listM) {
			if (item.getId().equals(id)) return item;
		}
		return null;
	}

	/**
	 * La m�thode add() permet d'ajouter un emplacement au camping
	 * @param p
	 * @return l'identifiant de l'emplacement
	 */
	public String add(Place p) {
		if (p!=null) {
			String idPlace  = UUID.randomUUID().toString();
			list.add(new Place(idPlace,p.getComment(), p.getType(), p.isWater(), p.isElect(), p.getLine(), p.getxPos(), p.getyPos(), p.getxSize(), p.getySize()));
			listM.add(new MPlace(idPlace,p.getComment(), p.getType(), p.isWater(), p.isElect(), p.getLine(),  p.getxPos(), p.getyPos(), p.getxSize(), p.getySize(), false, null));
			return idPlace;
		}
		else {
			return null;
		}
	}

	/**
	 * La m�thode init() permet d'initialiser la table avec si on veut ou non des emplacements
	 */
	public String init() {
		String idPlace  = UUID.randomUUID().toString();
		this.list.add(new Place(idPlace,"EP1", 1, false, false, 1,0,0,100,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP2", 1, false, false, 1, 85,0,100,200));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP3", 2, false, false, 1,-30,100,100,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP4", 2, false, false, 1, -45,200,200,100));
		
		this.list.add(new Place(UUID.randomUUID().toString(),"EP5", 2, false, false, 1, -60,350,200,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP6", 1, false, false, 1, -75, 450,100,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP7", 1, false, false, 1, 10,450,100,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP8", 2, false, false, 1,-105,550,200,100));

		this.list.add(new Place(UUID.randomUUID().toString(),"EP9", 2, false, false, 1,130,0,200,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP10", 1, false, false, 1,315,0,100,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP11", 1, false, false, 1,100,100,100,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP12", 2, false, false, 1,185,100,200,100));

		this.list.add(new Place(UUID.randomUUID().toString(),"EP13", 2, false, false, 1,70,250,200,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP14", 1, false, false, 1,255,250,100,100));
		
		this.list.add(new Place(UUID.randomUUID().toString(),"EP15", 2, false, false, 1,40,400,100,200));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP16", 1, false, false, 1,125,400,100,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP17", 1, false, false, 1,110,500,100,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP18", 2, false, false, 1,195,400,100,200));
		
		
		this.list.add(new Place(UUID.randomUUID().toString(),"EP19", 2, false, false, 1,330,0,100,200));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP20", 1, false, false, 1,415,0,100,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP21", 1, false, false, 1,400,100,100,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP22", 2, false, false, 1,485,0,100,200));
		
		this.list.add(new Place(UUID.randomUUID().toString(),"EP23", 2, false, false, 1,270,250,100,200));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP24", 1, false, false, 1,355,250,100,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP25", 1, false, false, 1,340,350,100,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP26", 2, false, false, 1,425,250,100,200));
		
		this.list.add(new Place(UUID.randomUUID().toString(),"EP27", 2, false, false, 1,210,500,200,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP28", 1, false, false, 1,395,500,100,100));
		
		this.list.add(new Place(UUID.randomUUID().toString(),"EP29", 2, false, false, 1,530,0,200,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP30", 1, false, false, 1,515,100,100,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP31", 1, false, false, 1,600,100,100,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP32", 2, false, false, 1,485,200,200,100));
		
		this.list.add(new Place(UUID.randomUUID().toString(),"EP33", 1, false, false, 1,470,350,100,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP34", 1, false, false, 1,455,450,100,100));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP35", 2, false, false, 1,540,350,100,200));
		this.list.add(new Place(UUID.randomUUID().toString(),"EP36", 2, false, false, 1,425,550,200,100));
		return idPlace;
	}

	/**
	 * La m�thode open() permet d'ouvrir le fichier places.ser qui contient les emplacements
	 */
	public void open() {
		try {
			FileInputStream fis = new FileInputStream("./Base/places.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);

			list.clear();
			listM.clear();
			int size = (int) ois.readObject();
			for(int i=0; i< size; i++) {
				Place obj  = (Place) ois.readObject();
				list.add(obj);
				listM.add(new MPlace(obj));
			}
			ois.close();

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * La m�thode close() permet de fermer le fichier places.ser qui contient les emplacements
	 */
	public void close() {
		try {
			FileOutputStream fos = new FileOutputStream("./Base/places.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			oos.writeObject(list.size());
			for (Place item: list) {
				oos.writeObject(item);
			}
			oos.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * La m�thode initBooked() permet de r�server ou non l'emplacement associ� � un client
	 */
	public void initBooked() {
		for(MPlace item : listM) {
			item.setBooked(false);
			item.setClientBooked("");
		}
	}
}
