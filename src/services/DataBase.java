package services;

import dataBase.Client;
import dataBase.Place;
import dataBase.Resa;
import model.*;

import java.util.*;
import java.util.List;

public class DataBase {

	/**
	 * Creer une variable cs de type ClientListService
	 */
	ClientListService cs;
	/**
	 * Creer une variable rs de type ResaListService
	 */
	ResaListService rs;
	/**
	 * Creer une variable ps de type PlaceListService
	 */
	PlaceListService ps;

	/**
	 * Creer un HashMap daySlotMap qui permet d'associer une date avec un jour
	 */
	Map<String, AgendaDaySlot> agendaDaySlotHashMap = new HashMap<String, AgendaDaySlot>();

	/**
	 * Constructeur de la classe DataBase qui permet d'initialiser les variables de liste
	 */
	public DataBase() {
		this.cs = new ClientListService();
		this.ps = new PlaceListService();
		this.rs = new ResaListService();
	}

	/**
	 * La m�thode getListClient() permet de retourner la liste des clients de type List
	 * @return la liste des clients de type List
	 */
	public List<Client> getListClient() {
		return this.cs.getList();
	}
  
	/**
	 * La m�thode getListPlace() permet de retourner la liste des emplacements de type List
	 * @return la liste des emplacements de type List
	 */
	public List<Place> getListPlace() {
		return this.ps.getList();
	}
  
	/**
	 * La m�thode getListMPlace() permet de retourner la liste des emplacements de type List (la m�thode retourne la liste affich�e dans l'interface graphique)
	 * @return la liste des emplacements de type List
	 */
	public List<MPlace> getListMPlace() {
		return this.ps.getListM();
	}
  
	/**
	 * La m�thode getListResa() permet de retourner la liste des r�servations de type List
	 * @return la liste des r�servations de type List
	 */
	public List<Resa> getListResa() {
		return this.rs.getList();
	}
  
	/**
	 * La m�thode getAgendaDaySlotHashMap() permet de retourner les associations entre la date et le jour
	 * @return les associations entre la date et le jour
	 */
	public Map<String, AgendaDaySlot> getAgendaDaySlotHashMap() { return agendaDaySlotHashMap; }

	/**
	 * La m�thode getClient() permet de retourner l'identifiant du client
	 * @return l'identifiant du client
	 */
	public Client getClient(String id) {
		return this.cs.get(id);
	}
  
	/**
	 * La m�thode getPlace() permet de retourner l'identifiant de l'emplacement
	 * @return l'identifiant de l'emplacement
	 */
	public Place getPlace(String id) {
		return this.ps.get(id);
	}
  
	/**
	 * La m�thode getPlaceM() permet de retourner l'identifiant de l'emplacement (celui qui est visible sur l'interface graphique)
	 * @return l'identifiant de l'emplacement (celui qui est visible sur l'interface graphique)
	 */
	public MPlace getPlaceM(String id) {
		return this.ps.getM(id);
	}	

	/**
	 * La m�thode addClient() permet de retourner un nouveau client
	 * @param c
	 * @return un nouveau client
	 */
	public String addClient(Client c) {
		return this.cs.add(c);
	}

	/**
	 * La m�thode addResa() permet de retourner une nouvelle r�servation
	 * @param c
	 * @return une nouvelle r�servation
	 */
	public String addResa(Resa r) {
		setTimeslots(r);
		return this.rs.add(r);
	}
  
	/**
	 * La m�thode addPlace() permet de retourner un nouvel emplacement
	 * @param c
	 * @return un nouvel emplacement
	 */
	public String addPlace(Place p) {
		return this.ps.add(p);
	}

	/**
	 * La m�thode init() permet d'initialiser les tables
	 */
	public void init() {
		this.rs.init(this.cs.init(), this.ps.init());
	}

	/**
	 * La m�thode open() permet d'ouvrir les fichiers .ser
	 */
	public void open() {
		this.cs.open();
		this.ps.open();
		this.rs.open();

		for (Resa item : this.rs.getList()) {
			setTimeslots(item);
		}
	}

	/**
	 * La m�thode setTimeslots() permet de 
	 * @param item
	 */
	public void setTimeslots(Resa item) {
		long diff = item.getEnd().getTime() - item.getStart().getTime();
		int nbDays =  (int) diff / 1000 / 60 / 60 / 24;

		Date startsAt = item.getStart();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startsAt);
		int y = calendar.get(Calendar.YEAR);
		int m= calendar.get(Calendar.MONTH)+1;
		int d = calendar.get(Calendar.DATE);
		String keyDate = setDateKey(startsAt);

		
		for(int i=0; i <= nbDays; i++) {
			AgendaDaySlot agendaDaySlot = agendaDaySlotHashMap.get(keyDate);
			// teste si le jour (cl� date) existe d�j� ? //
			if (agendaDaySlot != null) {
				Client objClient = getClient(item.getIdClient());
				Place objPlace = getPlace(item.getIdPlace());
				agendaDaySlot.getAgendaTimeSlots().add(new AgendaTimeSlot(objPlace.getId(),
						objClient.getId(),
						objPlace.getComment(),
						objClient.getName() + " " + objClient.getSurname()));
			} else {
        // sinon on le creer
				List<AgendaTimeSlot> agendaTimeSlots = new ArrayList<>();
				Client objClient = getClient(item.getIdClient());
				Place objPlace = getPlace(item.getIdPlace());
				agendaTimeSlots.add(new AgendaTimeSlot(objPlace.getId(),
                                                objClient.getId(),
                                                objPlace.getComment(),
                                                objClient.getName() + " " + objClient.getSurname()));
				agendaDaySlotHashMap.put(keyDate, new AgendaDaySlot(y, m, d, startsAt, agendaTimeSlots));
			}
			
			// incr�mente la Date de 1 jour //
			startsAt = setDatePlusOne(startsAt);
			calendar.setTime(startsAt);
			y = calendar.get(Calendar.YEAR);
			m= calendar.get(Calendar.MONTH)+1;
			d = calendar.get(Calendar.DATE);
			keyDate = setDateKey(startsAt);
		}
	}

	/**
	 * La m�thode close() permet de fermer les fichiers .ser
	 */
	public void close() {
		this.cs.close();
		this.rs.close();
		this.ps.close();
	}

	/**
	 * La m�thode initBooked() permet d'initialiser les emplacements r�serv�s
	 */
	public void initBooked() {
		this.ps.initBooked();
	}

	/**
	 * La m�thode isBooked() permet de rendre "r�serv�" l'emplacement
	 * @param date
	 */
	public void isBooked(Date date) {

		AgendaDaySlot agendaDaySlot = agendaDaySlotHashMap.get(setDateKey(date));
		if (agendaDaySlot != null) {
			for(AgendaTimeSlot item: agendaDaySlot.getAgendaTimeSlots()) {

				MPlace objPlace = getPlaceM(item.getIdPlace());
				objPlace.setBooked(true);
				objPlace.setClientBooked(item.getNameClient());
			}
		}
	}

	// -------------------------------------------------------------------------------------------//

	/**
	 * La m�thode setdatePlusOne() permet d'incr�menter la date de 1 jour
	 * @param start
	 * @return
	 */
	private Date setDatePlusOne(Date start) {

		// incr�mente la Date de 1 jour //
		Calendar c = Calendar.getInstance();
		c.setTime(start);
    	c.add(Calendar.DATE, 1);
    	start = c.getTime();
    	return start;
	}

	/**
	 * La m�thode setDateKey() permet de retourner la date sous forme de String
	 * @param start
	 * @return la date �crit de mani�re plus esth�tique
	 */
	private String setDateKey(Date start) {

		Calendar calendar = Calendar.getInstance();
		Date dateStart = start;
		calendar.setTime(dateStart);
		int y = calendar.get(Calendar.YEAR);
		int m= calendar.get(Calendar.MONTH)+1;
		int d = calendar.get(Calendar.DATE);

		String keyDate = Integer.toString(y);
		if (m < 10) keyDate += "-0" + m;
		else keyDate += "-" + m;
		if (d < 10) keyDate += "-0" + d;
		else keyDate += "-" + d;

		return keyDate;
	}	

	/**
	 * La m�thode paques() permet de retourner la date de p�ques en fonction de l'ann�e
	 * @param year
	 * @return la date de p�ques en fonction de l'ann�e
	 */
	public static Date paques(int year) {
		Calendar _calendar = GregorianCalendar.getInstance();  // creates a new calendar instance
		if (year < 1583) {
			throw new IllegalStateException();
		}
		int n = year % 19;
		int c = year / 100;
		int u = year % 100;
		int s = c / 4;
		int t = c % 4;
		int p = (c + 8) / 25;
		int q = (c - p + 1) / 3;
		int e = (19 * n + c - s - q + 15) % 30;
		int b = u / 4;
		int d = u % 4;
		int L = (32 + 2 * t + 2 * b - e - d) % 7;
		int h = (n + 11 * e + 22 * L) / 451;
		int m = (e + L - 7 * h + 114) / 31;
		int j = (e + L - 7 * h + 114) % 31;

		_calendar.set(Calendar.YEAR, year);
		_calendar.set(Calendar.MONTH, m - 1);
    	_calendar.set(Calendar.DATE, j + 1);

    	return _calendar.getTime();
	}

	/**
	 * La m�thode isDayOff() permet de retourner si le jour est f�ri� ou non
	 * @param date
	 * @return si le jour est f�ri� ou non
	 */
	public static boolean isDayOff(Date date) {
		Calendar _calendar = GregorianCalendar.getInstance();  // creates a new calendar instance
		_calendar.setTime(date);
		final int day = _calendar.get(Calendar.DAY_OF_MONTH);
		switch (_calendar.get(Calendar.MONTH)) {
		case Calendar.JANUARY:
			if (day == 1) { return true; }
			break;
		case Calendar.MAY:
			if (day == 1 || day == 8) { return true; }
			break;
		case Calendar.JULY:
			if (day == 14) { return true; }
			break;
		case Calendar.AUGUST:
			if (day == 15) { return true; }
			break;
		case Calendar.NOVEMBER:
			if (day == 1 || day == 11) { return true; }
			break;
		case Calendar.DECEMBER:
			if (day == 25) { return true; }
			break;
		default:
	}

    // Avant juillet on doit aussi v�rifier les f�tes li�es � P�ques
    if (_calendar.get(Calendar.MONTH) < 6) {
    	Date paques = paques(_calendar.get(Calendar.YEAR));
    	Calendar _calendarP = GregorianCalendar.getInstance();
    	_calendarP.setTime(paques);
    	int daysP = _calendarP.get(Calendar.DAY_OF_YEAR);
    	int daysD = _calendar.get(Calendar.DAY_OF_YEAR);
    	int days = daysD-daysP;
    	switch (days) {
    		case 0: // P�ques
    		case 1: // lundi de P�ques : 1 jour apr�s P�ques
    		case 39: // Ascension : 39 jours apr�s P�ques
    		case 49: // Pentec�te : 50 jours apr�s P�ques
    		case 50: // Lundi de Pentec�te : 50 jours apr�s P�ques
    		return true;
    		default:break;
    			}
   	}
    return false;
	}
}	
