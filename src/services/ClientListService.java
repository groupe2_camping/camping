package services;

import dataBase.Client;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ClientListService {

	/**
	 * Cr�e une variable list de type List
	 */
	private List<Client> list;

	/**
	 * Constructeur de la classe ClientListService, elle permet d'initialiser la list de clients de type ArrayList
	 */
	public ClientListService() {
		this.list = new ArrayList<>();
	}

	/**
	 * La m�thode getList() permet de retourner la liste des clients
	 * @return la liste des clients
	 */
	public List<Client> getList() {
		return list;
	}

	/**
	 * La m�thode get() permet de retourner l'identifaint d'un client
	 * @return
	 */
	public Client get(String id) {
		for(Client item : list) {
			if (item.getId().equals(id)) return item;
		}
		return null;
	}

	/**
	 * La m�thode add() permet d'ajouter un client dans les tables
	 * @param c
	 */
	public String add(Client c) {
		if (c!=null) {
			String idClient = UUID.randomUUID().toString();
			list.add(new Client(idClient, c.getName(), c.getSurname(), c.getPhone()));
			return idClient;
		}
		else {
			return null;
		}
	}

	/**
	 * La m�thode init() permet d'initialiser la table avec si on veut ou non des clients
	 */
	public String init() {
		String idClient = UUID.randomUUID().toString();
		this.list.add(new Client(idClient, "Flegeau", "Xavier","0634343432"));
		return idClient;
	}

	/**
	 * La m�thode open() permet d'ouvrir le fichier clients.ser qui contient les clients
	 * @return
	 */
	public int open() {
		try {
			FileInputStream fis = new FileInputStream("./Base/clients.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);

			list.clear();
			int size = (int) ois.readObject();
			for(int i=0; i< size; i++) {
				list.add((Client) ois.readObject());
			}
			ois.close();
			return list.size();

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * La m�thode close() permet de fermer le fichier clients.ser qui contient les clients
	 */
	public void close() {
		try {
			FileOutputStream fos = new FileOutputStream("./Base/clients.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			oos.writeObject(list.size());
			for (Client item: list) {
				oos.writeObject(item);
			}
			oos.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
