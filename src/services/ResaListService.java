package services;

import dataBase.Resa;

import java.io.*;
import java.util.*;

public class ResaListService {

	/**
	 * Cr�e une variable list de type List
	 */
	private List<Resa> list;

	/**
	 * Constructeur de la classe ResaListService qui permet d'initialiser les variables
	 */
	public ResaListService() {
  		this.list = new ArrayList<>();
  	}

	/**
	 * La m�thode getlist() permet de retourner la liste des r�servations
	 * @return la liste des r�servations
	 */
	public List<Resa> getList() {
  		return list;
  	}

	/**
	 * La m�thode add() permet d'ajouter une r�servation aux deux listes
	 * @param r
	 */
	public String add(Resa r) {
		if(r!=null) {
	  		String idResa = UUID.randomUUID().toString();
	  		list.add(new Resa(idResa, r.getIdClient(), r.getIdPlace(), r.getStart(), r.getEnd()));
	  		return idResa;
		}
		else {
			return null;
		}
  	}

  	/**
  	 * La m�thode init() permet d'initialiser les r�servations
  	 * @param idClient
  	 * @param idPlace
  	 */
	public void init(String idClient, String idPlace) {
  		Calendar calendarStart = GregorianCalendar.getInstance();  // creates a new calendar instance
  		calendarStart.set(Calendar.YEAR, 2020);
  		calendarStart.set(Calendar.MONTH, 4);
  		calendarStart.set(Calendar.DATE, 1);
  		calendarStart.set(Calendar.HOUR_OF_DAY, 0);
	    calendarStart.set(Calendar.MINUTE, 0);
	    calendarStart.set(Calendar.SECOND, 0);

	    Calendar calendarEnd = GregorianCalendar.getInstance();  // creates a new calendar instance
	    calendarEnd.set(Calendar.YEAR, 2020);
	    calendarEnd.set(Calendar.MONTH, 4);
	    calendarEnd.set(Calendar.DATE, 20);
	    calendarEnd.set(Calendar.HOUR_OF_DAY, 0);
	    calendarEnd.set(Calendar.MINUTE, 0);
	    calendarEnd.set(Calendar.SECOND, 0);

	    list.add(new Resa(UUID.randomUUID().toString(), idClient, idPlace, calendarStart.getTime(), calendarEnd.getTime()));
  	}

  	/**
	 * La m�thode open() permet d'ouvrir le fichier resa.ser qui contient les r�servations
	 */  
  	public void open() {
		  try {
			  FileInputStream fis = new FileInputStream("./Base/resa.ser");
			  ObjectInputStream ois = new ObjectInputStream(fis);
	
			  list.clear();
			  int size = (int) ois.readObject();
			  for(int i=0; i< size; i++) {
				  list.add((Resa) ois.readObject());
			  	}
			  ois.close();
	
		  } catch (IOException | ClassNotFoundException e) {
			  e.printStackTrace();
		  }
  	}

  	/**
	 * La m�thode close() permet de fermer le fichier resa.ser qui contient les r�servations
	 */
  	public void close() {
		  try {
			  FileOutputStream fos = new FileOutputStream("./Base/resa.ser");
			  ObjectOutputStream oos = new ObjectOutputStream(fos);

			  oos.writeObject(list.size());
			  for (Resa item: list) {
    	  oos.writeObject(item);
			  }
			  oos.close();

		  } catch (IOException e) {
			  e.printStackTrace();
		  }
  	}
}
