package testJUnit;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.jupiter.api.Test;

import dataBase.Resa;
import services.ResaListService;

class TestResaListService {

	@Test
	void testAdd() {
		ResaListService rls1 = new ResaListService();
		Resa r = null;
		rls1.add(r);
		assertTrue("Ajout non nul", rls1.getList().size()==0);
		Resa r2 = new Resa("test", "test", "test", new Date(), new Date());
		rls1.add(r2);
		assertTrue("Ajout fonctionnel", rls1.getList().size()==1);
	}

	@Test
	void testInit() {
		ResaListService rls1 = new ResaListService();
		rls1.init("test", "test");
		assertTrue("Initialisation fonctionelle", rls1.getList().size()==1);
	}

}
