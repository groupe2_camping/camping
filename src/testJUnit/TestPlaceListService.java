package testJUnit;

import static org.junit.Assert.assertTrue;
import services.PlaceListService;
import dataBase.Place;
import model.MPlace;

import org.junit.jupiter.api.Test;

class TestPlaceListService {

	@Test
	void testGet() {
		PlaceListService pls = new PlaceListService();
		Place p1 = new Place("test", "commentaire", 2, false, false, 1, 2, 3, 4, 5);
		String newID=pls.add(p1);
		assertTrue("Obtenir emplacement fonctionnel", pls.get(newID).getComment().equals(p1.getComment()));
	}

	@Test
	void testGetM() {
		PlaceListService pls = new PlaceListService();
		Place p1 = new Place("test", "commentaire", 2, false, false, 1, 2, 3, 4, 5);
		String newID=pls.add(p1);
		assertTrue("Obtenir emplacement r�serv� fonctionnel", pls.getM(newID).getComment().equals(p1.getComment()));
	}

	@Test
	void testAdd() {
		PlaceListService pls = new PlaceListService();
		Place p2=null;
		pls.add(p2);
		assertTrue("Emplacement non-nul", pls.getList().size()==0 && pls.getListM().size()==0);
		Place p1 = new Place("test", "commentaire", 2, false, false, 1, 2, 3, 4, 5);
		pls.add(p1);
		assertTrue("Ajouter emplacement fonctionnel", pls.getList().size()==1 && pls.getListM().size()==1);
	}

	@Test
	void testInit() {
		PlaceListService pls = new PlaceListService();
		pls.init();
		assertTrue("Initialisation fonctionnelle", pls.getList().size()==36);
	}

	@Test
	void testInitBooked() {
		PlaceListService pls = new PlaceListService();
		MPlace p1 = new MPlace("test", "commentaire", 2, false, false, 1, 2, 3, 4, 5, true, "test");
		String newID=pls.add(p1);
		pls.initBooked();
		assertTrue("Initialisation emplacements r�serv�s fonctionnel", pls.getM(newID).isBooked()==false);
	}

}
