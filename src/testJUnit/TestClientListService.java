package testJUnit;

import static org.junit.Assert.*;

import org.junit.jupiter.api.Test;

import dataBase.Client;
import services.*;

class TestClientListService {

	@Test
	void testGet() {
		ClientListService cls1 = new ClientListService();
		assertTrue("Get", cls1.get("sjjdj")==null);
		String id = cls1.init();
		assertTrue("C'est le bon client", cls1.get(id).getSurname().equals("Xavier"));
	}

	@Test
	void testAdd() {
		ClientListService cls1 = new ClientListService();
		Client n = null;
		cls1.add(n);
		assertTrue("Client nul pas ajout�", cls1.getList().size()==0);
		Client rv = new Client("hhe","Yves","Bougaud","0625154583");
		cls1.add(rv);
		assertTrue("Client ajout�", cls1.getList().size()==1);
	}

	@Test
	void testInit() {
		ClientListService cls2=new ClientListService();
		String id = cls2.init();
		assertTrue("Premier client ajout�", cls2.getList().size()==1);
		assertTrue("C'est le bon client", cls2.get(id).getSurname().equals("Xavier"));
	}

}
