package dataBase;

import java.io.Serializable;
import java.util.Date;

public class Resa implements Serializable {

	/**
	 * Cr�e une variable id de type String qui permet de donner un identifiant � la r�servation
	 */
	private String id;
	/**
	 * Cr�e une variable idClient de type String qui permet d'attribuer � la r�servation un identifiant pour le client
	 */
	private String idClient;
	/**
	 * Cr�e une variable idPlace de type String qui permet d'attribuer � la r�servation un identifiant pour l'emplacement
	 */
	private String idPlace;
	/**
	 * Cr�e une variable start de type Date qui permet de donner une date de d�but � la r�servation
	 */
	private Date start;
	/**
	 * Cr�e une variable end de type Date qui permet de donner une date de fin � la r�servation
	 */
	private Date end;

	/**
  	 * Constructeur de la classe Resa qui permet d'initialiser les r�servations
  	 * @param id
  	 * @param idClient
  	 * @param idPlace
  	 * @param start
  	 * @param end
  	 */
	public Resa() {}
	public Resa(String id, String idClient, String idPlace, Date start, Date end) {
		this.id = id;
		this.idClient = idClient;
		this.idPlace = idPlace;
		this.start = start;
		this.end = end;
	}

	/**
  	 * La m�thode getId() permet de retourner l'identifiant de la r�servation
  	 * @return l'identifiant de la r�servation
  	 */
	public String getId() {
		return id;
	}

	/**
  	 * La m�thode setId() permet de modifier l'identifiant de la r�servation
  	 * @param id
  	 */
	public Resa setId(String id) {
		this.id = id;
		return this;
	}

	/**
  	 * La m�thode getIdClient() permet de retourner l'identifiant du client attribu� � la r�servation
  	 * @return l'identifiant du client attribu� � la r�servation
  	 */
	public String getIdClient() {
		return idClient;
	}

	/**
  	 * La m�thode setIdClient() permet de modifier l'identifiant du client attribu� � la r�servation
  	 * @param idClient
  	 */
	public Resa setIdClient(String idClient) {
		this.idClient = idClient;
		return this;
	}

	/**
  	 * La m�thode getIdPlace() permet de retourner l'identifiant de l'emplacement attribu� � la r�servation
  	 * @return l'identifiant de l'emplacement attribu� � la r�servation
  	 */
	public String getIdPlace() {
		return idPlace;
	}

	/**
  	 * La m�thode setIdPlace() permet de modifier l'identifiant de l'emplacement attribu� � la r�servation
  	 * @param idPlace
  	 */
	public Resa setIdPlace(String idPlace) {
		this.idPlace = idPlace;
		return this;
	}

	/**
  	 * La m�thode getStart() permet de retourner la date de d�but de la r�servation
  	 * @return la date du d�but de la r�servation
  	 */
	public Date getStart() {
		return start;
	}

	/**
  	 * La m�thode setStart() permet de modifier la date de d�but de la r�servation
  	 * @param start
  	 */
	public Resa setStart(Date start) {
		this.start = start;
		return this;
	}

	/**
  	 * La m�thode getEnd() permet de retourner la date de fin de la r�servation
  	 * @return la date de fin de la r�servation
  	 */
	public Date getEnd() {
		return end;
	}

	/**
  	 * La m�thode setEnd() permet de retourner la date de fin de la r�servation
  	 * @param end
  	 */
	public Resa setEnd(Date end) {
		this.end = end;
		return this;
	}

	/**
  	 * La m�thode toString() permet de retourner toutes les informations concernant les r�servations
  	 */
	@Override
	public String toString() {
		return "Resa{" +
      "id=" + id +
      ", idClient=" + idClient +
      ", idPlace=" + idPlace +
      ", start=" + start +
      ", end=" + end +
      '}';
  }
}
