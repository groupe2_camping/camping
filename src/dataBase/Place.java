package dataBase;

import java.io.Serializable;

public class Place implements Serializable {

	/**
	 * Cr�e une variable id de type String qui permet de donner un identifiant � l'emplacement
	 */
	private String id;
	/**
	 * Cr�e une variable comment de type String qui permet d�finir le type de l'emplacement (mobil-home, chalet, etc.)
	 */
	private String comment;
	/**
	 * Cr�e une variable type de type int qui permet donner le nombre de personnes qui peuvent �tre accueillis sur l'emplacement 
	 */
	private int type;
	/**
	 * Cr�e une variable water de type boolean qui permet d'indiquer si l'emplacement contient de l'eau
	 */
	private boolean water;
	/**
	 * Cr�e une variable elect de type boolean qui permet d'indiquer si l'emplacement contient de l'�lectricit�
	 */
	private boolean elect;
	/**
	 * Cr�e une variable line de type int
	 */
	private int line;
	/**
	 * Cr�e une variable xPos de type int qui permet de donner la position en x de l'emplacement par rapport au plan du camping
	 */
	private int xPos;
	/**
	 * Cr�e une variable yPos de type int qui permet de donner la position en y de l'emplacement par rapport au plan du camping
	 */
	private int yPos;
	/**
	 * Cr�e une variable xSize de type int permet de donner la taille en x de l'emplacement
	 */
	private int xSize;
	/**
	 * Cr�e une variable ySize de type int qui permet de donner la taille en y de l'emplacement
	 */
	private int ySize;

	/**
	 * Constructeur de la classe Place qui permet d'initialiser les emplacements
	 * @param id
	 * @param comment
	 * @param type
	 * @param water
	 * @param elect
	 * @param line
	 * @param xPos
	 * @param yPos
	 * @param xSize
	 * @param ySize
	 */
	public Place() {}
	public Place(String id, String comment, int type, boolean water, boolean elect, int line, int xPos, int yPos, int xSize, int ySize) {
		this.id = id;
		this.comment = comment;
		this.type = type;
		this.water = water;
		this.elect = elect;
		this.line = line;
		this.xPos = xPos;
		this.yPos = yPos;
		this.xSize = xSize;
		this.ySize = ySize;
	}

	/**
	 * La m�thode getId() permet de retourner l'identifiant de l'emplacement
	 * @return l'identifiant de l'emplacement
	 */
	public String getId() {
		return id;
	}

	/**
	 * La m�thode setId() permet de modifier l'emplacement de l'emplacement
	 * @param id
	 */
	public Place setId(String id) {
		this.id = id;
		return this;
	}

	/**
	 * La m�thode getComment() permet de retourner le type de l'emplacement
	 * @return le type de l'emplacement
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * La m�thode setComment() permet de modifier le type de l'emplacement
	 * @param comment
	 */
	public Place setComment(String comment) {
		this.comment = comment;
		return this;
	}

	/**
	 * La m�thode getType() permet de retourner le nombre de personnes pouvant �tre accueillis sur l'emplacement
	 * @return le nombre de personnes pouvant �tre accueillis sur l'emplacement
	 */
	public int getType() {
		return type;
	}

	/**
	 * La m�thode setType() permet de modifier le nombre de personnes pouvant �tre accueillis sur l'emplacement
	 * @param type
	 */
	public Place setType(int type) {
		this.type = type;
		return this;
	}

	/**
	 * La m�thode isWater() permet de retourner si l'emplacement contient de l'eau
	 * @return true ou false selon si l'emplacement contient de l'eau
	 */
	public boolean isWater() {
		return water;
	}

	/**
	 * La m�thode setWater() permet de modifier si l'emplacement contient de l'eau
	 * @param water
	 */
	public Place setWater(boolean water) {
		this.water = water;
		return this;
  }

	/**
	 * La m�thode isElect() permet de retourner si l'emplacement contient de l'�lectrict�
	 * @return true ou false selon si l'emplacement contient de l'�lectricit�
	 */
	public boolean isElect() {
		return elect;
	}

	/**
	 * La m�thode setElect() permet de modifier si l'emplacement contient de l'�lectricit�
	 * @param elect
	 */
	public Place setElect(boolean elect) {
		this.elect = elect;
		return this;
	}

	/**
	 * La m�thode getxPos() permet de retourner la position en x de l'emplacement par rapport au plan du camping
	 * @return la position en x de l'emplacement par rapport au plan du camping
	 */
	public int getxPos() {
		return xPos;
	}

	/**
	 * La m�thode setxPos() permet de modifier la position en x de l'emplacement par rapport au plan du camping
	 * @param xPos
	 */
	public Place setxPos(int xPos) {
		this.xPos = xPos;
		return this;
	}

	/**
	 * La m�thode getyPos() permet de retourner la position en y de l'emplacement par rapport au plan du camping
	 * @return la position en y de l'emplacement par rapport au plan du camping
	 */
	public int getyPos() {
		return yPos;
	}

	/**
	 * La m�thode setyPos() permet de modifier la position en y de l'emplacement par rapport au plan du camping
	 * @param yPos
	 */
	public Place setyPos(int yPos) {
		this.yPos = yPos;
		return this;
	}

	/**
	 * La m�thode getxSize() permet de retourner la taille en x de l'emplacement
	 * @return la taille en x de l'emplacement
	 */
	public int getxSize() {
		return xSize;
	}

	/**
	 * La m�thode setxSize() permet de modifier la taille en x de l'emplacement
	 * @param xSize
	 */
	public Place setxSize(int xSize) {
		this.xSize = xSize;
		return this;
	}

	/**
	 * La m�thode getySize() permet de retourner la taille de y de l'emplacement
	 * @return la taille en y de l'emplacement
	 */
	public int getySize() {
		return ySize;
	}

	/**
	 * La m�thode setySize() permet de modifier la taille en y de l'emplacement
	 * @param ySize
	 */
	public Place setySize(int ySize) {
		this.ySize = ySize;
		return this;
	}

	/**
	 * La m�thode getLine() permet de retourner la variable line de type int
	 * @return la variable line de type int
	 */
	public int getLine() {
		return line;
	}

	/**
	 * La m�thode setLine() permet de modifier la variable line
	 * @param ySize
	 */
	public Place setLine(int line) {
		this.line = line;
		return this;
	}

	/**
	 * La m�thode toString() permet de retourner toutes les informations concernant les emplacements
	 */
	@Override
	public String toString() {
		return "Place{" +
      "id=" + id +
      ", comment='" + comment + '\'' +
      ", type=" + type +
      ", water=" + water +
      ", elect=" + elect +
      ", xPos=" + xPos +
      ", yPos=" + yPos +
      ", xSize=" + xSize +
      ", ySize=" + ySize +
      '}';
  }
}
