package dataBase;

import java.io.Serializable;

public class Client implements Serializable  {

	/**
	 * Cr�e une variable id de type String qui permet de donner un identifiant au client
	 */
	private String id;
	/**
	 * Cr�e une variable name de type String qui permet de donner un nom au client
	 */
	private String name;
	/**
	 * Cr�e une variable surname de type String qui permet de donner un pr�nom au client
	 */
	private String surname;
	/**
	 * Cr�e une variable phone de type String qui permet de donner un num�ro de t�l�phone au client
	 */
	private String phone;

	/**
	 * Constructeur de la classe Client qui permet d'initialiser les clients
	 * @param id
	 * @param name
	 * @param surname
	 * @param phone
	 */
	public Client() {}
	public Client(String id, String name, String surname, String phone) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.phone = phone;
	}

	/**
	 * La m�thode getId() permet de retourner l'identifiant du client
	 * @return l'identifiant du client
	 */
	public String getId() {
		return id;
	}

	/**
	 * La m�thode setId() permet de modifier l'identifiant du client
	 * @param id
	 */
	public Client setId(String id) {
		this.id = id;
		return this;
	}

	/**
	 * La m�thode getName() permet de retourner le nom du client
	 * @return le nom du client
	 */
	public String getName() {
		return name;
	}

	/**
	 * La m�thode setName() permet de modifier le nom du client
	 * @param name
	 */
	public Client setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * La m�thode getSurname() permet de retourner le pr�nom du client
	 * @return le pr�nom du client
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * La m�thode setSurname() permet de modifier le pr�nom du client
	 * @param surname
	 */
	public Client setSurname(String surname) {
		this.surname = surname;
		return this;
	}

	/**
	 * La m�thode getPhone() permet de retourner le num�ro de t�l�phone du client
	 * @return	le num�ro de t�l�phone du client
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * La m�thode setPhone() permet de modifier le num�ro de t�l�phone du client
	 * @param phone
	 */
	public Client setPhone(String phone) {
		this.phone = phone;
		return this;
	}

	/**
	 * La m�thode toString() permet de retourner toutes les informations concernant les clients
	 */
	@Override
	public String toString() {
		return "Client{" +
      "id=" + id +
      ", name='" + name + '\'' +
      ", surname='" + surname + '\'' +
      ", phone='" + phone + '\'' +
      '}';
  }
}
