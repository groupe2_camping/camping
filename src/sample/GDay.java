package sample;

import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.List;

public class GDay extends Parent {

	/**
	 * Cr�e une variable box de type Pane 
	 */
	Pane box = new Pane();
	Text texte  = new Text();

	/**
	 * Constructeur de la classe GDay qui permet de g�rer une case dans l'agenda
	 * @param date
	 * @param color
	 * @param listEmpl
	 * @param isBooked
	 */
	public GDay(String date, Paint color, List<String> listEmpl, boolean isBooked) {

	    texte.setText(date);
	    texte.setFont(new Font(16));
	    texte.setFill(Color.BLACK);
	    texte.setX(10);
	    texte.setY(20);
	    box.getChildren().add(texte);

	    if ((listEmpl != null) && !listEmpl.isEmpty()) {
	    	int i = 0;
	    	for (String item : listEmpl) {
		        Text texte1  = new Text();
		        texte1.setText(item);
		        texte1.setFont(new Font(12));
		        texte1.setFill(Color.BLACK);
		        texte1.setX(10);
		        texte1.setY(40 + i*20);
		        box.getChildren().add(texte1);
		        i++;
	    	}
	    	box.setBorder(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, new CornerRadii(10), new BorderWidths(3))));
	    } else if (isBooked)
	    	box.setBorder(new Border(new BorderStroke(Color.GREEN, BorderStrokeStyle.SOLID, new CornerRadii(10), new BorderWidths(3))));

	    	box.setBackground(new Background(new BackgroundFill(color, new CornerRadii(10), Insets.EMPTY)));

	    	this.getChildren().add(box);

	    	box.setPrefSize(150, 80);
	}
}
