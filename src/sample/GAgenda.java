package sample;

import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import model.AgendaDaySlot;
import model.AgendaTimeSlot;

import java.util.*;

public class GAgenda extends GridPane {

	/**
	 * Cr�e une variable days de type ArrayList
	 */
	private List<GDay> days = new ArrayList<>();
	/**
	 * Cr�e une variable mois de type int 
	 */
	private int mois;
	/**
	 * Cr�e une variable an de type int  
	 */
	private int an;

	/**
	 * Cnnstructeur de la classe GAgenda qui initialise l'agenda de l'IHM
	 * @param m
	 * @param y
	 */
	public GAgenda(int m, int y) {

		  this.mois = m;
		  this.an = y;

		  this.setTranslateX(25);
		  this.setTranslateY(25);

		  for (int jour = 1; jour <= 7; jour++) {
			  Text texte = new Text();
			  switch (jour) {
			  case 1:
				  texte.setText("Dimanche");
				  break;
			  case 2:
				  texte.setText("Lundi");
				  break;
			  case 3:
				  texte.setText("Mardi");
				  break;
			  case 4:
				  texte.setText("Mercredi");
				  break;
			  case 5:
				  texte.setText("Jeudi");
				  break;
			  case 6:
				  texte.setText("Vendredi");
				  break;
			  case 7:
				  texte.setText("Samedi");
				  break;
			  }
	      texte.setFont(new Font(20));
	      texte.setFill(Color.BLACK);
	      texte.setX(10);
	      texte.setY(10);
	      this.add(texte, jour, 0);
		  }

	    this.setHgap(10);
	    this.setVgap(10);

	    set(m, y, null, false);
	  }

	public List<GDay> getDays() {
		return days;
	}

	public void setDays(List<GDay> days) {
		this.days = days;
	}

	public int getMois() {
		return mois;
	}

	public void setMois(int mois) {
		this.mois = mois;
	}

	public int getAn() {
		return an;
	}

	public void setAn(int an) {
		this.an = an;
	}

	/**
	 * La methode remove() permet de supprimer les jours de tous un mois pour ne plus les afficher
	 */
	public void remove() {
		  for(GDay item : days) {
			  this.getChildren().remove(item);
		  }
	  }

	  private List<String> getListeEmpl(String keyDate,Map<String, AgendaDaySlot> map) {
		  List<String> listeEmpl = new ArrayList<>();
		  if (map != null) {
			  AgendaDaySlot agendaDaySlot = map.get(keyDate);
			  if (agendaDaySlot != null) {
				  for (AgendaTimeSlot itemTS : agendaDaySlot.getAgendaTimeSlots()) {
					  listeEmpl.add(itemTS.getNamePlace() + "-" + itemTS.getNameClient());
				  }
			  }
		  }
		  return listeEmpl;
	  }

	 /**
	  * La m�thode setDatePlusOne() permet d'incrementer la date de 1 jour
	  * @param start
	  * @return la date mise � jour
	  */
	  private Date setDatePlusOne(Date start) {

	    // incremente la Date de 1 jour //
	    Calendar c = Calendar.getInstance();
	    c.setTime(start);
	    c.add(Calendar.DATE, 1);
	    start = c.getTime();
	    return start;
	  }

	  /**
	   * La m�thode setDateKey() permet de retourner la date en String et d'une maniere plus conventionelle
	   * @param start
	   * @return keyDate
	   */
	  private String setDateKey(Date start) {

	    Calendar calendar = Calendar.getInstance();
	    Date dateStart = start;
	    calendar.setTime(dateStart);
	    int y = calendar.get(Calendar.YEAR);
	    int m= calendar.get(Calendar.MONTH)+1;
	    int d = calendar.get(Calendar.DATE);

	    String keyDate = Integer.toString(y);
	    if (m < 10) keyDate += "-0" + m;
	    else keyDate += "-" + m;
	    if (d < 10) keyDate += "-0" + d;
	    else keyDate += "-" + d;

	    return keyDate;
	  }

	 /**
	  * La m�thode set() permet d'initialiser le calendrier de l'agenda
	  * @param mois
	  * @param year
	  * @param map
	  * @param isBooked
	  */
	  public void set(int mois, int year, Map<String, AgendaDaySlot> map, boolean isBooked) {

	    Calendar calendar = GregorianCalendar.getInstance();
	    calendar.set(Calendar.YEAR, year);
	    calendar.set(Calendar.MONTH, mois);
	    calendar.set(Calendar.DATE, 1);
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    Date dateFirst = calendar.getTime();
	    int dayFirst = calendar.get(Calendar.DAY_OF_WEEK);      // DIM 1 LUN 2 ... SAM 7
	
	    Date dateEnd = getEndsOn(year, mois+1);
	    calendar.setTime(dateEnd);
	    int dateLast = calendar.get(Calendar.DATE);
	    int dayLast = calendar.get(Calendar.DAY_OF_WEEK);
	
	    Date dateStart = dateFirst;
	    String keyDate = setDateKey(dateStart);
	    int date = 1;

	    // 1ere semaine //
	    for (int i= dayFirst; i <= 7; i++) {
	    	Paint color = Color.DARKGREY;
	    	if ((i == 1) || (i == 7)) color = Color.GRAY;
	
	    	GDay day = new GDay( Integer.toString(date++), color, getListeEmpl(keyDate, map), isBooked);
	    	days.add(day);
	    	this.add(day, i, 1);
	
	    	// incremente la Date de 1 jour //
	    	dateStart = setDatePlusOne(dateStart);
	    	keyDate = setDateKey(dateStart);
	    }

	    // 2 - 3 - 4
	    for (int j=2; j<5; j++)
	    	for (int i= 1; i <= 7; i++) {
	    		Paint color = Color.DARKGREY;
	    		if ((i == 1) || (i == 7)) color = Color.GRAY;

	    		GDay day = new GDay( Integer.toString(date++), color, getListeEmpl(keyDate, map), isBooked);
	    		days.add(day);
	    		this.add(day, i, j);

	    		// incremente la Date de 1 jour //
	    		dateStart = setDatePlusOne(dateStart);
	    		keyDate = setDateKey(dateStart);
	    	}

	    // 5
	    int reste = dateLast - date + 1;
	    if (reste <= 7) {
	    	for (int i = 1; i <= reste; i++) {
	    		Paint color = Color.DARKGREY;
	    		if ((i == 1) || (i == 7)) color = Color.GRAY;

	    		GDay day = new GDay( Integer.toString(date++), color, getListeEmpl(keyDate, map), isBooked);
	    		days.add(day);
	    		this.add(day, i, 5);

	    		// incremente la Date de 1 jour //
	    		dateStart = setDatePlusOne(dateStart);
	    		keyDate = setDateKey(dateStart);
	    	}
	    } else {
	    	for (int i = 1; i <= 7; i++) {
	    		Paint color = Color.DARKGREY;
	    		if ((i == 1) || (i == 7)) color = Color.GRAY;

	    		GDay day = new GDay( Integer.toString(date++), color, getListeEmpl(keyDate, map), isBooked);
	    		days.add(day);
	    		this.add(day, i, 5);

	        // incremente la Date de 1 jour //
	        dateStart = setDatePlusOne(dateStart);
	        keyDate = setDateKey(dateStart);
	    	}

	    	int reste1 = dateLast - date + 1;
	    	for (int i = 1; i <= reste1; i++) {
	    		Paint color = Color.DARKGREY;
	    		if ((i == 1) || (i == 7)) color = Color.GRAY;

	    		GDay day = new GDay( Integer.toString(date++), color, getListeEmpl(keyDate, map), isBooked);
	    		days.add(day);
	    		this.add(day, i, 6);

	        // incremente la Date de 1 jour //
	        dateStart = setDatePlusOne(dateStart);
	        keyDate = setDateKey(dateStart);
	    	}
	    }
  }

	/**
	 * La m�thode getEndsOn() permet de donner le jour de fin de mois
	 * @param year
	 * @param month
	 * @return
	 */
	  public Date getEndsOn(int year, int month) {

	    Calendar calendarEnd = GregorianCalendar.getInstance();  // creates a new calendar instance
	    calendarEnd.set(Calendar.HOUR, 0);
	    calendarEnd.set(Calendar.MINUTE, 0);
	    calendarEnd.set(Calendar.SECOND, 0);
	    calendarEnd.set(Calendar.YEAR, year);
	    calendarEnd.set(Calendar.MONTH, month - 1);
	    switch (month - 1) {
	    case 0:
	    	calendarEnd.set(Calendar.DATE, 31);
	    	break;
	    case 1:
	    	if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
	    		calendarEnd.set(Calendar.DATE, 29);
	    	} else {
	    		calendarEnd.set(Calendar.DATE, 28);
	    	}
	    	break;
	    case 2:
	    	calendarEnd.set(Calendar.DATE, 31);
	    	break;
	    case 3:
	    	calendarEnd.set(Calendar.DATE, 30);
	    	break;
	    case 4:
	    	calendarEnd.set(Calendar.DATE, 31);
	    	break;
	    case 5:
	    	calendarEnd.set(Calendar.DATE, 30);
	    	break;
	    case 6:
	    	calendarEnd.set(Calendar.DATE, 31);
	    	break;
	    case 7:
	    	calendarEnd.set(Calendar.DATE, 31);
	    	break;
	    case 8:
	    	calendarEnd.set(Calendar.DATE, 30);
	    	break;
	    case 9:
	    	calendarEnd.set(Calendar.DATE, 31);
    	  	break;
	    case 10:
	    	calendarEnd.set(Calendar.DATE, 30);
	    	break;
	    case 11:
	    	calendarEnd.set(Calendar.DATE, 31);
	    	break;
	    }
	    return calendarEnd.getTime();
	  }
}
