package sample;

import dataBase.Place;
import javafx.event.EventHandler;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.Parent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class GPlace extends Parent {

	/**
	 * Creer une variable space de type int
	 */
	private int space = 15;
	/**
	 * Creer une variable positionX de type int 
	 */
	private int positionX = 5;
	/**
	 * Creer une variable positionY de type int 
	 */ 
	private int positionY = 20;

	 /**
	  * Creer et initialise une rectangle 
	  */
	  Rectangle rectangle = new Rectangle();
	  Text texte  = new Text();
	  Text texte1  = new Text();

	  /**
	   * Constructeur de la classe GPlace qui permet de g�rer la carte du camping
	   * @param place
	   * @param position
	   * @param paint
	   * @param name
	   */
	  public GPlace(Place place, int position, Paint paint, String name) {

	    rectangle.setX(place.getxPos() + space * (position+1));
	    rectangle.setY(place.getyPos() + space);
	    rectangle.setWidth(place.getxSize());
	    rectangle.setHeight(place.getySize());
	    rectangle.setArcHeight(10);
	    rectangle.setArcWidth(10);
	    rectangle.setFill(paint);

	    this.getChildren().add(rectangle);

	    texte.setText(place.getComment());
	    texte.setFont(new Font(16));
	    texte.setFill(Color.BLACK);
	    texte.setX(place.getxPos() + space * (position+1) + positionX);
	    texte.setY(place.getyPos() + space + positionY);
	    this.getChildren().add(texte);

	    if (!name.equals("")) {
	    	texte1.setText(name);
	    	texte1.setFont(new Font(16));
	    	texte1.setFill(Color.BLACK);
	    	texte1.setX(place.getxPos() + space * (position + 1) + positionX);
	    	texte1.setY(place.getyPos() + space + positionY + 20);
	    	this.getChildren().add(texte1);
	   	}

	    this.setOnMousePressed(new EventHandler<MouseEvent>(){
	    	public void handle(MouseEvent me){
	    		appuyer();
	    	}
	    });
	    this.setOnMouseReleased(new EventHandler<MouseEvent>(){
	    	public void handle(MouseEvent me){
	    		relacher();
	    	}
	    });
	  }

  	/**
  	 * La m�thode appuyer() permet de mettre l'emplacement voulu en beige lors de l'appui
  	 */
	public void appuyer(){
      rectangle.setFill(Color.BEIGE);
  	}

  	public void relacher(){

  	}
}
