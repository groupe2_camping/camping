package sample;

import dataBase.Place;
import javafx.scene.Parent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import model.MPlace;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class GCamping extends Parent {

	private List<GPlace> places = new ArrayList<>();
	Text texte  = new Text();

	public GCamping(List<MPlace> list) {

	    this.setTranslateX(25);
	    this.setTranslateY(25);
	
	    texte.setText("Pas de Filtre");
	    texte.setFont(new Font(16));
	    texte.setFill(Color.BLACK);
	    texte.setX(0);
	    texte.setY(0);
	    this.getChildren().add(texte);
	
	    set(list);
	}

	public void set(List<MPlace> list) {
		int i = 0;
		int line = 1;
		for (MPlace item : list) {
			if (item.getLine() != line) i=0;

			Paint paint = Color.GRAY;
			if (item.isBooked())
				paint = Color.RED;
			
			if (item.getClientBooked() == null) {
				GPlace place = new GPlace(item, i++, paint, "");
				places.add(place);
				this.getChildren().add(place);
			} else {
				GPlace place = new GPlace(item, i++, paint, item.getClientBooked());
				places.add(place);
				this.getChildren().add(place);
			}

			line = item.getLine();
		}
	}

	public void setCamping(Date date, List<MPlace> list) {

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);

		if (date == null)
			texte.setText("Pas de Filtre");
		else {
			texte.setText(simpleDateFormat.format(date));
		}

		for (GPlace item: places) {
			this.getChildren().remove(item);
		}
		set(list);
	}
}
