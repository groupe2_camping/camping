package model;

import java.util.Date;
import java.util.List;

public class AgendaDaySlot {

	/**
	 * Cr�e une variable an de type int qui g�re l'ann�e
	 */
	private int an;
	/**
	 * Cr�e une variable mois de type int qui g�re le mois
	 */
	private int mois;
	/**
	 * Cr�e une variable jour de type int qui g�re le jour
	 */
	private int jour;
	/**
	 * Cr�e une variable key de type Date qui g�re la date
	 */
	private Date key;
	/**
	 * Cr�e une variable agendaTimeSlots de type List qui g�re les r�servations de l'agenda
	 */
	private List<AgendaTimeSlot> agendaTimeSlots;

	/**
	 * Constructeur de la classe AgendaDaySlot qui permet d'initialiser les variables
	 * @param an
	 * @param mois
	 * @param jour
	 * @param key
	 * @param agendaTimeSlots
	 */
	public AgendaDaySlot(int an, int mois, int jour, Date key, List<AgendaTimeSlot> agendaTimeSlots) {
		this.an = an;
		this.mois = mois;
		this.jour = jour;
		this.key = key;
		this.agendaTimeSlots = agendaTimeSlots;
	}


	/**
	 * La m�thode getJour() permet de retourner le num�ro du jour
	 * @return le num�ro du jour
	 */
	public int getJour() {
		return jour;
	}

	/**
	 * La m�thode setJour() permet de modifier le num�ro du jour
	 * @param jour
	 * @return
	 */
	public AgendaDaySlot setJour(int jour) {
		this.jour = jour;
		return this;
	}

	/**
	 * La m�thode getAn() permet de retourner l'ann�e
	 * @return l'ann�e
	 */
	public int getAn() {
		return an;
	}
	
	/**
	 * La m�thode setAn() permet de modifier l'ann�e
	 * @param an
	 * @return
	 */
	public AgendaDaySlot setAn(int an) {
		this.an = an;
		return this;
	}
	
	/**
	 * La m�thode getMois() permet de retourner le num�ro du mois
	 * @return le num�ro du mois
	 */
	public int getMois() {
		return mois;
	}

	/**
	 * La m�thode setMois() permet de modifier le num�ro du mois
	 * @param mois
	 * @return
	 */
	public AgendaDaySlot setMois(int mois) {
		this.mois = mois;
		return this;
	}	

	/**
	 * La m�thode getKey() permet de retourner la date
	 * @return
	 */
	public Date getKey() {
		return key;
	}

	/**
	 * La m�thode setKey() permet de modifier la date
	 * @param key
	 * @return
	 */
	public AgendaDaySlot setKey(Date key) {
		this.key = key;
		return this;
	}

	/**
	 * La m�thode getAgendaTimeSlots() permet de retourner les r�servations de l'agenda
	 * @return les r�servations de l'agenda
	 */
	public List<AgendaTimeSlot> getAgendaTimeSlots() {
		return agendaTimeSlots;
	}

	/**
	 * La m�thode setAgendaTimeSlots() permet de modifier les r�servations de l'agenda
	 * @param agendaTimeSlots
	 * @return
	 */
	public AgendaDaySlot setAgendaTimeSlots(List<AgendaTimeSlot> agendaTimeSlots) {
		this.agendaTimeSlots = agendaTimeSlots;
		return this;
	}
}
