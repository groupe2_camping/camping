package model;

import dataBase.Place;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.List;

public class TablePlace extends Parent {

	/**
	 * Cr�e une variable listData de type ObservableList
	 */
	private ObservableList<TablePlaceData> listData = FXCollections.observableArrayList();
	/**
	 * Cr�e une variable table de type TableView
	 */
	private TableView<TablePlaceData> table = new TableView<>();
  
	/**
	 * Constructeur de la classe TablePlace qui permet d'initialiser le variables
	 * @param list
	 */
	public TablePlace(List<Place> list) {

		if ((list != null) && (list.size() != 0)) {
			for (Place item : list) {
				listData.add(new TablePlaceData(item.getId(),
                                        		item.getComment(),
		                                        item.getType(),
		                                        item.isWater(),
		                                        item.isElect(),
		                                        item.getxPos(),
		                                        item.getyPos(),
		                                        item.getxSize(),
		                                        item.getySize()));
			}
		}

	    TableColumn<TablePlaceData, String> idP = new TableColumn<>("idEmplacement");
	    TableColumn<TablePlaceData, String> comment = new TableColumn<>("Commentaire");
	    TableColumn<TablePlaceData, Integer> type = new TableColumn<>("typeEmplacement");
	    TableColumn<TablePlaceData, Boolean> water = new TableColumn<>("Eau");
	    TableColumn<TablePlaceData, Boolean> elect = new TableColumn<>("Elect.");
	    TableColumn<TablePlaceData, Integer> xPos = new TableColumn<>("xPosition");
	    TableColumn<TablePlaceData, Integer> yPos = new TableColumn<>("yPosition");
	    TableColumn<TablePlaceData, Integer> xSize = new TableColumn<>("Largeur");
	    TableColumn<TablePlaceData, Integer> ySize = new TableColumn<>("Longueur");

	    idP.setCellValueFactory(new PropertyValueFactory("id"));
	    comment.setCellValueFactory(new PropertyValueFactory("comment"));
	    type.setCellValueFactory(new PropertyValueFactory("type"));
	    water.setCellValueFactory(new PropertyValueFactory("water"));
	    elect.setCellValueFactory(new PropertyValueFactory("elect"));
	    xPos.setCellValueFactory(new PropertyValueFactory("xPos"));
	    yPos.setCellValueFactory(new PropertyValueFactory("yPos"));
	    xSize.setCellValueFactory(new PropertyValueFactory("xSize"));
	    ySize.setCellValueFactory(new PropertyValueFactory("ySize"));

	    table.getColumns().setAll(idP, comment,type,water, elect, xPos, yPos, xSize, ySize);
	    table.setItems(listData);
	}

	/**
	 * La m�thode getListdata() permet de retourner la liste des emplacements
	 * @return la liste des emplacements
	 */
	public ObservableList<TablePlaceData> getListData() {
		return this.listData;
	}
  
	/**
	 * La m�thode getTable() permet de retourner la liste des emplacements 
	 * @return la liste des emplacements
	 */
	public TableView<TablePlaceData> getTable() {return this.table;}

	/**
	 * La m�thode add() permet d'ajouter un emplacement � la liste des emplacements
	 * @param p
	 */
	public void add(Place p) {
		listData.add(new TablePlaceData(p.getId(),p.getComment(), p.getType(), p.isWater(),
					p.isElect(), p.getxPos(), p.getyPos(), p.getxSize(), p.getySize()));
	}
}
