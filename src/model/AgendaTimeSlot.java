package model;

public class AgendaTimeSlot {

	/**
	 * Creer une variable idPlace de type String qui permet de donner un identifiant � l'emplacement
	 */
	private String idPlace;
	/**
	 * Creer une variable idClient de type String qui permet de donner un identifiant au client
	 */
	private String idClient;
	/**
	 * Creer une variable namePlace de type String qui permet de donner un nom � l'emplacement
	 */
	private String namePlace;
	/**
	 * Creer une variable nameClient de type String qui permet de donner un nom au client
	 */
	private String nameClient;

	/**
	 * Constructeur de la classe AgendaTimeSlot qui permet d'initialiser les variables
	 * @param idPlace
	 * @param idClient
	 * @param namePlace
	 * @param nameClient
	 */
	public AgendaTimeSlot(String idPlace, String idClient, String namePlace, String nameClient) {
		this.idPlace = idPlace;
		this.idClient = idClient;
		this.namePlace = namePlace;
		this.nameClient = nameClient;
	}

	/**
	 * La m�thode getIdPlace() permet de retourner l'identifiant de l'emplacement
	 * @return l'identifiant de l'emplacement
	 */
	public String getIdPlace() {
		return idPlace;
	}

	/**
	 * La m�thode setIdPlace() permet de modifier l'identifiant de l'emplacement
	 * @param idPlace
	 * @return
	 */
	public AgendaTimeSlot setIdPlace(String idPlace) {
		this.idPlace = idPlace;
		return this;
	}

	/**
	 * La m�thode getIdClient() permet de retourner l'identifiant du client
	 * @return l'identifiant du client
	 */
	public String getIdClient() {
		return idClient;
	}

	/**
	 * La m�thode setIdClient() permet de modifier l'identifiant du client
	 * @param idClient
	 * @return l'identifiant du client
	 */
	public AgendaTimeSlot setIdClient(String idClient) {
		this.idClient = idClient;
		return this;
	}

	/**
	 * La m�thode getNamePlace() permet de retourner le nom de l'emplacement
	 * @return le nom de l'emplacement
	 */
	public String getNamePlace() {
		return namePlace;
	}

	/**
	 * La m�thode setNamePlace() permet de modifier le nom de l'emplacement
	 * @param namePlace
	 * @return
	 */
	public AgendaTimeSlot setNamePlace(String namePlace) {
		this.namePlace = namePlace;
		return this;
	}

	/**
	 * La m�thode getNameClient() permet de retourner le nom du client
	 * @return
	 */
	public String getNameClient() {
		return nameClient;
	}

	/**
	 * La m�thode setNameClient() permet de modifier le nom du client
	 * @param nameClient
	 * @return
	 */
	public AgendaTimeSlot setNameClient(String nameClient) {
		this.nameClient = nameClient;
		return this;
	}
}
