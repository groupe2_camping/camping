package model;

import javafx.beans.property.*;

public class MPlaceTable {

	/**
	 * Cr�e une variable id de type StringProperty qui permet de donner un identifiant
	 */
	private StringProperty id;
	/**
	 * Cr�e une variable comment de type StringProperty qui permet d�finir le type
	 */
	private StringProperty comment;
	/**
	 * Cr�e une variable type de type IntegerProperty qui permet donner le nombre de personnes qui peuvent �tre accueillis
	 */
	private IntegerProperty type;
	/**
	 * Cr�e une variable water de type BooleanProperty qui permet d'indiquer si il y a de l'eau
	 */
	private BooleanProperty water;
	/**
	 * Cr�e une variable elect de type BooleanProperty qui permet d'indiquer si il y a de l'�lectricit�
	 */
	private BooleanProperty elect;
	/**
	 * Cr�e une variable xPos de type IntegerProperty qui permet de donner la position en x
	 */
	private IntegerProperty xPos;
	/**
	 * Cr�e une variable yPos de type IntegerProperty qui permet de donner la position en y
	 */
	private IntegerProperty yPos;
	/**
	 * Cr�e une variable xSize de type IntegerProperty permet de donner la taille en x
	 */
	private IntegerProperty xSize;
	/**
	 * Cr�e une variable ySize de type IntegerProperty permet de donner la taille en y
	 */
	private IntegerProperty ySize;

	/**
	 * Constructeur de la classe MPlaceTable qui permet d'initialiser les variables
	 * @param id
	 * @param comment
	 * @param type
	 * @param water
	 * @param elect
	 * @param xPos
	 * @param yPos
	 * @param xSize
	 * @param ySize
	 */
	public MPlaceTable(String id, String comment, int type, boolean water, boolean elect, int xPos, int yPos, int xSize, int ySize) {
		this.id = new SimpleStringProperty(id);
		this.comment = new SimpleStringProperty(comment);
		this.type = new SimpleIntegerProperty(type);
		this.water = new SimpleBooleanProperty(water);
		this.elect = new SimpleBooleanProperty(elect);
		this.xPos = new SimpleIntegerProperty(xPos);
		this.yPos = new SimpleIntegerProperty(yPos);
		this.xSize = new SimpleIntegerProperty(xSize);
		this.ySize = new SimpleIntegerProperty(ySize);
	}

	/**
	 * La m�thode getId() permet de retourner l'identifiant de type String
	 * @return l'identifiant de type String
	 */
	public String getId() {
		return id.get();
	}

	/**
	 * La m�thode idProperty() permet de retourner l'identifiant de type StringProperty
	 * @return l'identifiant de type StringProperty
	 */
	public StringProperty idProperty() {
		return id;
	}

	/**
	 * La m�thode setId() permet de modifier l'identifiant
	 * @param id
	 */
	public void setId(String id) {
		this.id.set(id);
	}

	/**
	 * La m�thode getComment() permet de retourner le type de type String
	 * @return le type de type String
	 */
	public String getComment() {
		return comment.get();
	}

	/**
	 * La m�thode commentProperty() permet de retourner le type de type StringProperty
	 * @return le type de type StringProperty
	 */
	public StringProperty commentProperty() {
		return comment;
	}

	/**
	 * La m�thode setComment() permet de modifier le type
	 * @param comment
	 */
	public void setComment(String comment) {
		this.comment.set(comment);
	}

	/**
	 * La m�thode getType() permet de retourner le nombre de personnes pouvant �tre accueillis de type int
	 * @return le nomre de personnes pouvant �tre accueillis de type int
	 */
	public int getType() {
		return type.get();
	}

	/**
	 * La m�thode typeProperty() permet de retourner le nombre de personnes pouvant �tre accueillis de type IntegerProperty
	 * @return le nombre de personnes pouvant �tre accueillis de type IntegerProperty
	 */
	public IntegerProperty typeProperty() {
		return type;
	}

	/**
	 * La m�thode setType() permet de modifier le nombre de personnes pouvant �tre accueillis
	 * @param type
	 */
	public void setType(int type) {
		this.type.set(type);
	}

	/**
	 * La m�thode isWater() permet de retourner si il y a de l'eau de type boolean
	 * @return true ou false selon si il y a de l'eau de type boolean
	 */
	public boolean isWater() {
		return water.get();
	}

	/**
	 * La m�thode waterProperty() permet de retourner si il y a de l'eau de type BooleanProperty
	 * @return true ou false selon si il y a de l'eau de type BooleanProperty
	 */
	public BooleanProperty waterProperty() {
		return water;
	}

	/**
 	 * La m�thode setWater() permet de modifier si il y a de l'eau
 	 * @param water
 	 */
	public void setWater(boolean water) {
		this.water.set(water);
	}

	/**
 	 * La m�thode isElect() permet de retourner si il y a de l'�lectricit� de type boolean
 	 * @return true ou false si il y a de l'�lectricit� de type boolean
 	 */
	public boolean isElect() {
		return elect.get();
	}

	/**
 	 * La m�thode electProperty() permet de retourner si il y a de l'�lectricit� de type BooleanProperty
 	 * @return true ou false selon si il y a de l'�lectricit� de type BooleanProperty
 	 */
	public BooleanProperty electProperty() {
		return elect;
	}

	/**
 	 * La m�thode setElect() permet de modifier si il y a de l'�lectricit�
 	 * @param elect
 	 */
	public void setElect(boolean elect) {
		this.elect.set(elect);
	}

	/**
 	 * La m�thode getxPos() permet de retourner la position en x de type int
 	 * @return la position en x de type int
 	 */
	public int getxPos() {
		return xPos.get();
	}

	/**
 	 * La m�thode xPosProperty() permet de retourner la position en x de type IntegerProperty
 	 * @return la position en x de type IntegerProperty
 	 */
	public IntegerProperty xPosProperty() {
		return xPos;
	}

	/**
 	 * La m�thode setxPos() permet de modifier la position en x
 	 * @param xPos
 	 */
	public void setxPos(int xPos) {
		this.xPos.set(xPos);
	}

	/**
 	 * La m�thode getyPos() permet de retourner la position en y de type int
 	 * @return la position en y de type int
 	 */
	public int getyPos() {
		return yPos.get();
	}

	/**
	 * La m�thode getyPos() permet de retourner la position en y de type IntegerProperty
	 * @return la position en y de type IntegerProperty
	 */
	public IntegerProperty yPosProperty() {
		return yPos;
	}

	/**
 	 * La m�thode setyPos() permet de modifier la position en y
 	 * @param yPos
 	 */
	public void setyPos(int yPos) {
		this.yPos.set(yPos);
	}

	/**
 	 * La m�thode getxSize() permet de retourner la taille en x de type int
 	 * @return la taille en x de type int
 	 */
	public int getxSize() {
		return xSize.get();
	}

	/**
 	 * La m�thode xSizeProperty() permet de retourner la taille en x de type IntegerProperty
 	 * @return la taile en x de type IntegerProperty
 	 */
	public IntegerProperty xSizeProperty() {
		return xSize;
	}

	/**
 	 * La m�thode setxSize() permet de modifier la taille en x
 	 * @param xSize
 	 */
	public void setxSize(int xSize) {
		this.xSize.set(xSize);
	}

 	/**
 	 * La m�thode getySize() permet de retourner la taille en y de type int
 	 * @return la taille en y de type int
 	 */
	public int getySize() {
		return ySize.get();
	}

	/**
 	 * La m�thode ySizeProperty() permet de retourner la taille en y de type IntegerProperty
 	 * @return la taille en y de type IntegerProperty
 	 */
	public IntegerProperty ySizeProperty() {
		return ySize;
	}

	/**
 	 * La m�thode setySize() permet de modifier la taille en y
 	 * @param ySize
 	 */
	public void setySize(int ySize) {
		this.ySize.set(ySize);
	}
}
