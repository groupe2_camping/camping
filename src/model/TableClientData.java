package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TableClientData {

	/**
	 * Cr�e une variable id de type StringProperty
	 */
	private StringProperty id;
	/**
	 * Cr�e une variable name de type String Property
	 */
	private StringProperty name;
	/**
	 * Cr�e une variable surname de type String Property
	 */
	private StringProperty surname;
	/**
	 * Cr�e une variable phone de type StringProperty
	 */
	private StringProperty phone;

	/**
	 * Constructeur de la classe TableClientData qui permet d'initialiser les variables
	 * @param id
	 * @param name
	 * @param surname
	 * @param phone
	 */
	public TableClientData(String id, String name, String surname, String phone) {
		this.id = new SimpleStringProperty(id);
		this.name = new SimpleStringProperty(name);
		this.surname = new SimpleStringProperty(surname);
		this.phone = new SimpleStringProperty(phone);
	}

	/**
	 * La m�thode getId() permet de retourner l'identifiant du client de type String
	 * @return l'identifiant du client de type String
	 */
	public String getId() {
		return id.get();
	}

	/**
	 * La m�thode idProperty() permet de retourner l'identifiant du client de type StringProperty
	 * @return l'identifiant du client de type StringProperty
	 */
	public StringProperty idProperty() {
		return id;
	}

	/**
	 * La m�thode setId() permet de modifier l'identifiant du client
	 * @param id
	 */
	public void setId(String id) {
		this.id.set(id);
	}

	/**
	 * La m�thode getName() permet de retourner le nom du client de type String
	 * @return le nom du client de type String
	 */
	public String getName() {
		return name.get();
	}

	/**
	 * La m�thode nameProperty() permet de retourner le nom du client de type StringProperty
	 * @return le nom du client de type StringProperty
	 */
	public StringProperty nameProperty() {
		return name;
	}

	/**
	 * La m�thode setName() permet de modifier le nom du client
	 * @param name
	 */
	public void setName(String name) {
		this.name.set(name);
	}

	/**
	 * La m�thode getSurname() permet de retourner le pr�nom du client de type String
	 * @return le pr�nom du client de type String
	 */
	public String getSurname() {
		return surname.get();
	}

	/**
	 * La m�thode surnameProperty() permet de retourner le pr�nom du client de type StringProperty
	 * @return le pr�nom du client de type StringProperty
	 */
	public StringProperty surnameProperty() {
		return surname;
	}

	/**
	 * La m�thode setSurname() permet de modifier le pr�nom du client
	 * @param surname
	 */
	public void setSurname(String surname) {
		this.surname.set(surname);
	}

	/**
	 * La m�thode getPhone() permet de retourner le num�ro de t�l�phone du client de type String
	 * @return	le num�ro de t�l�phone du client de type String
	 */
	public String getPhone() {
		return phone.get();
	}

	/**
	 * La m�thode phoneProperty() permet de retourner le num�ro de t�l�phone du client de type StringProperty
	 * @return	le num�ro de t�l�phone du client detype StringProperty
	 */
	public StringProperty phoneProperty() {
		return phone;
	}

	/**
	 * La m�thode setPhone() permet de modifier le num�ro de t�l�phone du client
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone.set(phone);
	}
}
