package model;

import dataBase.Client;
import dataBase.Place;

public class MPlace extends Place {

	/**
	 * Cr�e une variable isBooked de type boolean
	 */
	private boolean isBooked;       // local variable
	/**
	 * Cr�e une variable clientBooked de type String
	 */
	private String clientBooked;    // local variable

	/**
	 * 1er constrcteur de la classe MPlace qui permet d'initialiser les variables (m�me si les constructeurs sont cod�s diff�remment, ils permettent d'obtenir le m�me r�sultat) 
	 * @param id
	 * @param comment
	 * @param type
	 * @param water
	 * @param elect
	 * @param xPos
	 * @param yPos
	 * @param xSize
	 * @param ySize
	 * @param isBooked
	 * @param clientBooked
	 */
	public MPlace(Place obj) {
		super(obj.getId(), obj.getComment(), obj.getType(), obj.isWater(), obj.isElect(), obj.getLine(), obj.getxPos(), obj.getyPos(), obj.getxSize(), obj.getySize());
		this.isBooked = false;
		this.clientBooked = "";
	}
 
	/**
	 * 2nd constructeur de la classe MPlace qui permet d'initialiser les variables (m�me si les constructeurs sont cod�s diff�remment, ils permettent d'obtenir le m�me r�sultat)
	 * @param obj
	 * @param isBooked
	 * @param clientBooked
	 */
	public MPlace(String id, String comment, int type, boolean water, boolean elect, int line, int xPos, int yPos, int xSize, int ySize, boolean isBooked, String clientBooked) {
		super(id, comment, type, water, elect, line, xPos, yPos, xSize, ySize);
		this.isBooked = isBooked;
    	this.clientBooked = clientBooked;
	}

	/**
	 * La m�thode isBooked() permet de retourner si l'emplacement est r�serv� ou non
	 * @return si l'emplacement est r�serv� ou non
	 */
	public boolean isBooked() {
		return isBooked;
	}

	/**
	 * La m�thode setBooked() permet de modifier si l'emplacement est r�serv� ou non
	 * @param booked
	 */
	public MPlace setBooked(boolean booked) {
		isBooked = booked;
		return this;
	}

	/**
	 * La m�thode getClientBooked() permet de retourner si le client a r�serv� ou non
	 * @return si le client a r�serv� ou non
	 */
	public String getClientBooked() {
		return clientBooked;
	}

	/**
	 * La m�thode setClientBooked() permet de modifier si le client a r�serv� ou non
	 * @param booked
	 */
	public MPlace setClientBooked(String clientBooked) {
		this.clientBooked = clientBooked;
		return this;
	}
}
