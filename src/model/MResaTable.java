package model;

import javafx.beans.property.*;

import java.util.Date;

public class MResaTable {

	/**
	 * Cr�e une variable id de type StringProperty qui permet de donner l'identifiant
	 */
	private StringProperty id;
	/**
	 * Cr�e une variable idClient de type StringProperty qui permet d'attribuer un identfiant � un client
	 */
	private StringProperty idClient;
	/**
	 * Cr�e une variable idPlace de type StringProperty qui permet d'attribuer un identifiant � l'emplacement
	 */
	private StringProperty idPlace;
	/**
	 * Cr�e une variable start de type ObjectProperty qui permet de donner une date de d�but
	 */
	private ObjectProperty<Date> start = new SimpleObjectProperty<>();
	/**
	 * Cr�e une variable end de type ObjectProperty qui permet de donner une date de fin
	 */
	private ObjectProperty<Date> end = new SimpleObjectProperty<>();

	/**
	 * Constructeur de la classe MResaTable qui permet d'initialiser les variables
	 * @param id
	 * @param idClient
	 * @param idPlace
	 * @param start
	 * @param end
	 */
	public MResaTable(String id, String idClient, String idPlace, Date start, Date end) {
		this.id = new SimpleStringProperty(id);
		this.idClient = new SimpleStringProperty(idClient);
		this.idPlace = new SimpleStringProperty(idPlace);
    	this.start = new SimpleObjectProperty<>();
    	this.start.set(start);
    	this.end = new SimpleObjectProperty<>();
    	this.end.set(end);
	}

	/**
	 * La m�thode getId() permet de retourner l'identifiant de type String
	 * @return l'identifiant de type String
	 */
	public String getId() {
		return id.get();
	}

	/**
	 * La m�thode idProperty() permet de retourner l'identifiant de type StringProperty
	 * @return l'identifiant de type StringProperty
	 */
	public StringProperty idProperty() {
		return id;
	}

	/**
	 * La m�thode setId() permet de modifier l'identifiant
	 * @param id
	 */
	public void setId(String id) {
		this.id.set(id);
	}

	/**
	 * La m�thode getIdClient() permet de retourner l'identifiant du client de type String
	 * @return l'identifiant du client de type String
	 */
	public String getIdClient() {
		return idClient.get();
	}	

	/**
	 * La m�thode idClientProperty() permet de retourner l'identifiant du client de type StringProperty
	 * @return l'identifiant du client de type StringProperty
	 */
	public StringProperty idClientProperty() {
		return idClient;
	}

	/**
	 * La m�thode setIdClient() permet de modifier l'identifiant du client
	 * @param idClient
	 */
	public void setIdClient(String idClient) {
		this.idClient.set(idClient);
	}

	/**
	 * La m�thode getIdPlace() permet de retourner l'identifiant de l'emplacement de type String
	 * @return l'identifiant de l'emplacement de type String
	 */
	public String getIdPlace() {
		return idPlace.get();
	}

	/**
	 * La m�thode idPlaceProperty() permet de retourner l'identifiant de l'emplacement de type StringProperty
	 * @return l'identfiant de l'emplacement de type StringProperty
	 */
	public StringProperty idPlaceProperty() {
		return idPlace;
	}

	/**
	 * La m�thode setIdPlace() permet de modifier l'identifiant de l'emplacement
	 * @param idPlace
	 */
	public void setIdPlace(String idPlace) {
		this.idPlace.set(idPlace);
	}

	/**
	 * La m�thode getStart() permet de retourner la date de d�but de type Date
	 * @return la date de d�but de type Date
	 */
	public Date getStart() {
		return start.get();
	}

	/**
	 * La m�thode startProperty() permet de retourner la date de d�but de type ObjectProperty
	 * @return la date de d�but de type ObjectProperty
	 */
	public ObjectProperty<Date> startProperty() {
		return start;
	}

	/**
	 * La m�thode setStart() permet de modifier la date de d�but
	 * @param start
	 */
	public void setStart(Date start) {
		this.start.set(start);
	}

	/**
	 * La m�thode getEnd() permet de retourner la date de fin de type Date
	 * @return la date de fin de type Date
	 */
	public Date getEnd() {
		return end.get();
	}

	/**
	 * La m�thode endProperty() permet de retourner la date de fin de type ObjectProperty
	 * @return la date de fin de type ObjectProperty
	 */
	public ObjectProperty<Date> endProperty() {
		return end;
	}	

	/**
	 * La m�thode setEnd() permet de modifier la date de fin
	 * @param end
	 */
	public void setEnd(Date end) {
		this.end.set(end);
	}
}
