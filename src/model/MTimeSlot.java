package model;

public class MTimeSlot {

	/**
	 * Cr�e une variable idPlace de type int qui peremt d'attribuer un identifiant � un emplacement 
	 */
	private int idPlace;
	/**
	 * Cr�e une variable idClient de type int qui permet d'attribuer un identifiant � un client
	 */
	private int idClient;

	/**
	 * Constructeur de la classe MTImeSlot qui permet d'initialiser les variables
	 * @param idPlace
	 * @param idClient
	 */
	public MTimeSlot(int idPlace, int idClient) {
		this.idPlace = idPlace;
		this.idClient = idClient;
	}

	/**
	 * La m�thode getIdPlace() permet de retourner l'identifiant de l'emplacement
	 * @return l'identifiant de l'emplacement
	 */
	public int getIdPlace() {
		return idPlace;
	}

	/**
	 * La m�thode setIdPlace() permet de modifier l'identifiant de l'emplacement
	 * @param idPlace
	 */
	public MTimeSlot setIdPlace(int idPlace) {
		this.idPlace = idPlace;
		return this;
	}

	/**
	 * La m�thode getIdClient() permet de retourner l'identifiant du client
	 * @return l'identifiant du client
	 */
	public int getIdClient() {
		return idClient;
	}

	/**
	 * La m�thode setIdClient() permet de modifier l'identifiant du client
	 * @param idClient
	 */
	public MTimeSlot setIdClient(int idClient) {
		this.idClient = idClient;
		return this;
	}
}
