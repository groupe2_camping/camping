package model;

import javafx.beans.property.*;

public class TablePlaceData {

	/**
	 * Cr�e une variable id de type String qui permet de donner un identifiant � l'emplacement
	 */  
	private StringProperty id;
	/**
	 * Cr�e une variable comment de type String qui permet d�finir le type de l'emplacement (mobil-home, chalet, etc.)
	 */  
	private StringProperty comment;
	/**
	 * Cr�e une variable type de type int qui permet donner le nombre de personnes qui peuvent �tre accueillis sur l'emplacement 
	 */  
	private IntegerProperty type;
	/**
	 * Cr�e une variable water de type boolean qui permet d'indiquer si l'emplacement contient de l'eau
	 */ 
	private BooleanProperty water;
	/**
	 * Cr�e une variable elect de type boolean qui permet d'indiquer si l'emplacement contient de l'�lectricit�
	 */ 
	private BooleanProperty elect;
	/**
	 * Cr�e une variable xPos de type int qui permet de donner la position en x de l'emplacement par rapport au plan du camping
	 */
	private IntegerProperty xPos;
	/**
	 * Cr�e une variable yPos de type int qui permet de donner la position en y de l'emplacement par rapport au plan du camping
	 */ 
	private IntegerProperty yPos;
	/**
	 * Cr�e une variable xSize de type int permet de donner la taille en x de l'emplacement
	 */ 
	private IntegerProperty xSize;
	/**
	 * Cr�e une variable ySize de type int qui permet de donner la taille en y de l'emplacement
	 */ 
	private IntegerProperty ySize;

	/**
	 * Constructeur de la classe TablePlaceData permet d'initialiser les variables 
	 * @param id
	 * @param comment
	 * @param type
	 * @param water
	 * @param elect
	 * @param xPos
	 * @param yPos
	 * @param xSize
	 * @param ySize
	 */
	public TablePlaceData(String id, String comment, int type, boolean water, boolean elect, int xPos, int yPos, int xSize, int ySize) {
		  this.id = new SimpleStringProperty(id);
		  this.comment = new SimpleStringProperty(comment);
		  this.type = new SimpleIntegerProperty(type);
		  this.water = new SimpleBooleanProperty(water);
		  this.elect = new SimpleBooleanProperty(elect);
		  this.xPos = new SimpleIntegerProperty(xPos);
		  this.yPos = new SimpleIntegerProperty(yPos);
		  this.xSize = new SimpleIntegerProperty(xSize);
		  this.ySize = new SimpleIntegerProperty(ySize);
	  }

	/**
	 * La m�thode getId() permet de retourner l'identifiant de l'emplacement de type String
	 * @return l'identifiant de l'emplacement de type String
	 */
	public String getId() {
		  return id.get();
	  }

	/**
	 * La m�thode idProperty() permet de retourner l'identifiant de l'emplacement de type StringProperty
	 * @return l'identifiant de l'emplacement de type StringProperty
	 */
	public StringProperty idProperty() {
		  return id;
	  }

	/**
	 * La m�thode setId() permet de modifier l'emplacement de l'emplacement
	 * @param id
	 */ 
	public void setId(String id) {
		  this.id.set(id);
	  }

	/**
	 * La m�thode getComment() permet de retourner le type de l'emplacement de type String
	 * @return le type de l'emplacement de type String
	 */ 
	public String getComment() {
		  return comment.get();
	  }

	/**
	 * La m�thode commentProperty() permet de retourner le type de l'emplacement de type String Property
	 * @return le type de l'emplacement de type StringProperty
	 */  
	public StringProperty commentProperty() {
		  return comment;
	  }

	/**
	 * La m�thode setComment() permet de modifier le type de l'emplacement
	 * @param comment
	 */  
	public void setComment(String comment) {
		  this.comment.set(comment);
	  }

	/**
	 * La m�thode getType() permet de retourner le nombre de personnes pouvant �tre accueillis sur l'emplacement de type int
	 * @return le nombre de personnes pouvant �tre accueillis sur l'emplacement de type int
	 */  
	public int getType() {
		  return type.get();
	  }

	/**
	 * La m�thode typeProperty() permet de retourner le nombre de personnes pouvant �tre accueillis sur l'emplacement de type IntegerProperty
	 * @return le nombre de personnes pouvant �tre accueillis sur l'emplacement de type IntegerProperty
	 */
	public IntegerProperty typeProperty() {
	  return type;
	  }

	/**
	 * La m�thode setType() permet de modifier le nombre de personnes pouvant �tre accueillis sur l'emplacement
	 * @param type
	 */  
	public void setType(int type) {
		  this.type.set(type);
	  }

	/**
	 * La m�thode isWater() permet de retourner si l'emplacement contient de l'eau de type boolean
	 * @return true ou false selon si l'emplacement contient de l'eau de type boolean
	 */  
	public boolean isWater() {
		  return water.get();
	  }

	/**
	 * La m�thode waterProperty() permet de retourner si l'emplacement contient de l'eau detype BooleanProperty
	 * @return true ou false selon si l'emplacement contient de l'eau de type BooleanProperty
	 */  
	public BooleanProperty waterProperty() {
		  return water;
	  }

	/**
	 * La m�thode setWater() permet de modifier si l'emplacement contient de l'eau
	 * @param water
	 */  
	public void setWater(boolean water) {
		  this.water.set(water);
	  }

	/**
	 * La m�thode isElect() permet de retourner si l'emplacement contient de l'�lectricit� de type boolean
	 * @return true ou false selon si l'emplacement contient de l'�lectricit� de type boolean
	 */ 
	public boolean isElect() {
		  return elect.get();
	  }

	/**
	 * La m�thode electProperty() permet de retourner si l'emplacement contient de l'�lectricit� de type BooleanProperty
	 * @return true ou false selon si l'emplacement contient de l'�lectricit� de type BooleanProperty
	 */  
	public BooleanProperty electProperty() {
		  return elect;
	  }

	/**
	 * La m�thode setElect() permet de modifier si l'emplacement contient de l'�lectricit�
	 * @param elect
	 */  
	public void setElect(boolean elect) {
		  this.elect.set(elect);
	  }

	/**
	 * La m�thode getxPos() permet de retourner la position en x de l'emplacement par rapport au plan du camping de type int
	 * @return la position en x de l'emplacement par rapport au plan du camping de type int
	 */ 
	public int getxPos() {
		  return xPos.get();
	  }

	/**
	 * La m�thode xPosProperty() permet de retourner la position en x de l'emplacement par rapport au plan du camping de type IntegerProperty
	 * @return la position en x de l'emplacement par rapport au plan du camping de type IntegerProperty
	 */  
	public IntegerProperty xPosProperty() {
		  return xPos;
	  }

	/**
	 * La m�thode setxPos() permet de modifier la position en x de l'emplacement par rapport au plan du camping
	 * @param xPos
	 */  
	public void setxPos(int xPos) {
		  this.xPos.set(xPos);
	  }

	/**
	 * La m�thode getyPos() permet de retourner la position en y de l'emplacement par rapport au plan du camping de type int
	 * @return la position en y de l'emplacement par rapport au plan du camping de type int
	 */ 
	public int getyPos() {
		  return yPos.get();
	  }

	/**
	 * La m�thode yPosProperty() permet de retourner la position en y de l'emplacement par rapport au plan du camping de type IntegerProperty
	 * @return la position en y de l'emplacement par rapport au plan du camping de type IntegerProperty
	 */  
	public IntegerProperty yPosProperty() {
		  return yPos;
	  }

	/**
	 * La m�thode setyPos() permet de modifier la position en y de l'emplacement par rapport au plan du camping
	 * @param yPos
	 */
	public void setyPos(int yPos) {
		  this.yPos.set(yPos);
	  }

	/**
	 * La m�thode getxSize() permet de retourner la taille en x de l'emplacement de type int
	 * @return la taille en x de l'emplacement de type int
	 */ 
	public int getxSize() {
		  return xSize.get();
	  }

	/**
	 * La m�thode xSizeProperty() permet de retourner la taille en x de l'emplacement de type IntegerProperty
	 * @return la taille en x de l'emplacement de type IntegerProperty
	 */  
	public IntegerProperty xSizeProperty() {
		  return xSize;
	  }

	/**
	 * La m�thode setxSize() permet de modifier la taille en x de l'emplacement
	 * @param xSize
	 */ 
	public void setxSize(int xSize) {
		  this.xSize.set(xSize);
	  }

	/**
	 * La m�thode getySize() permet de retourner la taille de y de l'emplacement de type int
	 * @return la taille en y de l'emplacement de type int
	 */ 
	public int getySize() {
		  return ySize.get();
	  }

	/**
	 * La m�thode ySizeProperty() permet de retourner la taille de y de l'emplacement de type IntegerProperty
	 * @return la taille en y de l'emplacement de type IntegerProperty
	 */ 
	public IntegerProperty ySizeProperty() {
		  return ySize;
	  }

	/**
	 * La m�thode setySize() permet de modifier la taille en y de l'emplacement
	 * @param ySize
	 */  
	public void setySize(int ySize) {
		  this.ySize.set(ySize);
	  }
}
