package model;

import dataBase.Client;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

import java.util.List;

public class TableClient extends Parent {

	/**
	 * Cr�e une variable listData de type ObservableList
	 */
	private ObservableList<TableClientData> listData = FXCollections.observableArrayList();
	/**
	 * Cr�e une variable table de type TableView
	 */
	private TableView<TableClientData> table = new TableView<>();

	/**
	 * Constructeur de la classe TableClient qui permet initialiser le tableau des clients sur l'interface graphique
	 * @param list
	 */
	public TableClient(List<Client> list) {

		if ((list != null) && (list.size() != 0)) {
			for (Client item : list) {
				listData.add(new TableClientData(item.getId(), item.getName(), item.getSurname(), item.getPhone()));
			}
		}

		TableColumn<TableClientData, String> id = new TableColumn<>("idClient");
		TableColumn<TableClientData, String> name = new TableColumn<>("Nom");
		TableColumn<TableClientData, String> surname = new TableColumn<>("Pr�nom");
		TableColumn<TableClientData, String> phone = new TableColumn<>("T�l�phone");

		id.setCellValueFactory(new PropertyValueFactory("id"));
	    name.setCellValueFactory(new PropertyValueFactory("name"));
	    surname.setCellValueFactory(new PropertyValueFactory("surname"));
	    phone.setCellValueFactory(new PropertyValueFactory("phone"));

	    table.getColumns().setAll(id, name,surname,phone);
	    table.setItems(this.listData);
	}

	/**
	 * La m�thode getListData() permet de retourner la liste des clients
	 * @return la liste des clients
	 */
	public ObservableList<TableClientData> getListData() {
		return this.listData;
	}
  
	/**
	 * La m�thode getTable() permet de retourner la liste des clients
	 * @return la liste des clients
	 */
	public TableView<TableClientData> getTable() {return this.table;}

	/**
	 * La m�thode add() permet d'ajouter un client dans la liste des clients
	 * @param c
	 */
	public void add(Client c) {
		listData.add(new TableClientData(c.getId(), c.getName(), c.getSurname(), c.getPhone()));
	}
}
