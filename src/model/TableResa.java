package model;

import dataBase.Resa;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.List;

public class TableResa extends Parent  {

	/**
	 * Cr�e une variable listData de type ObservableList
	 */
	private ObservableList<TableResaData> listData = FXCollections.observableArrayList();
	/**
	 * Cr�e une variable table de type TableView
	 */
	private TableView<TableResaData> table = new TableView<>();

	/**
	 * Constructeur de la classe TableResa qui permet d'initialiser les variables
	 * @param list
	 */
	public TableResa(List<Resa> list) {

		if ((list != null) && (list.size() != 0)) {
			for (Resa item : list) {
				listData.add(new TableResaData(item.getId(), item.getIdClient(), item.getIdPlace(), item.getStart(), item.getEnd()));
			}
		}
		
	    TableColumn<TableResaData, String> idR = new TableColumn<>("idResa");
	    TableColumn<TableResaData, String> idClient = new TableColumn<>("idClient");
	    TableColumn<TableResaData, String> idPlace = new TableColumn<>("idEmplacement");
	    TableColumn<TableResaData, String> start = new TableColumn<>("D�but");
	    TableColumn<TableResaData, String> end = new TableColumn<>("Fin");

	    idR.setCellValueFactory(new PropertyValueFactory("id"));
	    idClient.setCellValueFactory(new PropertyValueFactory("idClient"));
	    idPlace.setCellValueFactory(new PropertyValueFactory("idPlace"));
	    start.setCellValueFactory(new PropertyValueFactory("start"));
	    end.setCellValueFactory(new PropertyValueFactory("end"));
	
	    table.getColumns().setAll(idR, idClient,idPlace,start, end);
	    table.setItems(listData);
	}

	/**
	 * La m�thode getListData() permet de retourner la liste des r�servations
	 * @return la liste des r�servations
	 */
	public ObservableList<TableResaData> getListData() { return this.listData; }
	
	/**
	 * La m�thode getListData() permet de retourner la liste des r�servations
	 * @return la liste des r�servations
	 */
	public TableView<TableResaData> getTable() {return this.table;}

	/**
	 * La m�thode add() permet d'ajouter une r�servation dans la liste des r�servations
	 * @param r
	 */
	public void add(Resa r) {
	  listData.add(new TableResaData(r.getId(), r.getIdClient(), r.getIdPlace(), r.getStart(), r.getEnd()));
	}
}
