package model;

import java.util.Date;
import java.util.List;

public class MDaySlot {

	/**
	 * Cr�e une variable key de type Date
	 */
	private Date key;
	/**
	 * Cr�e une liste de MTimeSlot
	 */
	private List<MTimeSlot>  timeSlots;

	/**
	 * Constructeur de la classe MDaySlot qui permet d'initialiser les variables
	 * @param key
	 * @param timeSlots
	 */
	public MDaySlot(Date key, List<MTimeSlot> timeSlots) {
		this.key = key;
		this.timeSlots = timeSlots;
	}

	/**
	 * La m�thode getKey() permet de retourner 
	 * @return
	 */
	public Date getKey() {
		return key;
	}

	/**
	 * La m�thode setKey() permet de modifier
	 * @param key
	 */
	public MDaySlot setKey(Date key) {
		this.key = key;
		return this;
	}

	/**
	 * La m�thode getTimeSlots() permet de retourner la liste de MTimeSlot
	 * @return
	 */
	public List<MTimeSlot> getTimeSlots() {
		return timeSlots;
	}

	/**
	 * La m�thode setTimeSlots() permet de modifier la liste de MTimeSlot
	 * @param timeSlots
	 */
	public MDaySlot setTimeSlots(List<MTimeSlot> timeSlots) {
		this.timeSlots = timeSlots;
		return this;
	}
}
