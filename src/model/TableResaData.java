package model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Date;

public class TableResaData {

	/**
	 * Cr�e une variable id de type String qui permet de donner un identifiant � la r�servation
	 */ 
	private StringProperty id;
	/**
	 * Cr�e une variable idClient de type String qui permet d'attribuer � la r�servation un identifiant pour le client
	 */ 
	private StringProperty idClient;
	/**
	 * Cr�e une variable idPlace de type String qui permet d'attribuer � la r�servation un identifiant pour l'emplacement
	 */ 
	private StringProperty idPlace;
	/**
	 * Cr�e une variable start de type Date qui permet de donner une date de d�but � la r�servation
	 */ 
	private ObjectProperty<Date> start = new SimpleObjectProperty<>();
	/**
	 * Cr�e une variable end de type Date qui permet de donner une date de fin � la r�servation
	 */  
	private ObjectProperty<Date> end = new SimpleObjectProperty<>();

	/**
	 * Constructeur de la classe TableResaData  qui permet d'initialiser la variables
	 * @param id
	 * @param idClient
	 * @param idPlace
	 * @param start
	 * @param end
	 */
	public TableResaData(String id, String idClient, String idPlace, Date start, Date end) {
	    this.id = new SimpleStringProperty(id);
	    this.idClient = new SimpleStringProperty(idClient);
	    this.idPlace = new SimpleStringProperty(idPlace);
	    this.start = new SimpleObjectProperty<>();
	    this.start.set(start);
	    this.end = new SimpleObjectProperty<>();
	    this.end.set(end);
	  }

	/**
  	 * La m�thode getId() permet de retourner l'identifiant de la r�servation de type String
  	 * @return l'identifiant de la r�servation de type String
  	 */  
	public String getId() {
		  return id.get();
	  }

	/**
  	 * La m�thode idProperty() permet de retourner l'identifiant de la r�servation de type StringProperty
  	 * @return l'identifiant de la r�servation de type StringProperty
  	 */  
	public StringProperty idProperty() {
		  return id;
	  }

	/**
  	 * La m�thode setId() permet de modifier l'identifiant de la r�servation
  	 * @param id
  	 */ 
	public void setId(String id) {
		  this.id.set(id);
	  }

	/**
  	 * La m�thode getIdClient() permet de retourner l'identifiant du client attribu� � la r�servation de type String
  	 * @return l'identifiant du client attribu� � la r�servation de type String
  	 */ 
	public String getIdClient() {
		  return idClient.get();
	  }

	/**
  	 * La m�thode idClientProperty() permet de retourner l'identifiant du client attribu� � la r�servation de type StringProperty
  	 * @return l'identifiant du client attribu� � la r�servation de type String Property
  	 */
	public StringProperty idClientProperty() {
		  return idClient;
	  }

	/**
  	 * La m�thode setIdClient() permet de modifier l'identifiant du client attribu� � la r�servation
  	 * @param idClient
  	 */  
	public void setIdClient(String idClient) {
		  this.idClient.set(idClient);
	  }

	/**
  	 * La m�thode getIdPlace() permet de retourner l'identifiant de l'emplacement attribu� � la r�servation de type String
  	 * @return l'identifiant de l'emplacement attribu� � la r�servation de type String
  	 */  
	public String getIdPlace() {
		  return idPlace.get();
	  }

	/**
  	 * La m�thode idPlaceProperty() permet de retourner l'identifiant de l'emplacement attribu� � la r�servation de type StringProperty
  	 * @return l'identifiant de l'emplacement attribu� � la r�servation de type StringProperty
  	 */
	public StringProperty idPlaceProperty() {
		  return idPlace;
	  }	

	/**
  	 * La m�thode setIdPlace() permet de modifier l'identifiant de l'emplacement attribu� � la r�servation
  	 * @param idPlace
  	 */  
	public void setIdPlace(String idPlace) {
		  this.idPlace.set(idPlace);
	  }

	/**
  	 * La m�thode getStart() permet de retourner la date de d�but de la r�servation de type Date
  	 * @return la date du d�but de la r�servation de type Date
  	 */
	public Date getStart() {
		  return start.get();
	  }

	/**
  	 * La m�thode startProperty() permet de retourner la date de d�but de la r�servation de type ObjectProperty
  	 * @return la date du d�but de la r�servation de type ObjectProperty
  	 */ 
	public ObjectProperty<Date> startProperty() {
		  return start;
	  }

	/**
  	 * La m�thode setStart() permet de modifier la date de d�but de la r�servation
  	 * @param start
  	 */ 
	public void setStart(Date start) {
		  this.start.set(start);
	  }

	/**
  	 * La m�thode getEnd() permet de retourner la date de fin de la r�servation de type Date
  	 * @return la date de fin de la r�servation de type Date
  	 */  
	public Date getEnd() {
		  return end.get();
	  }

	/**
  	 * La m�thode endProperty() permet de retourner la date de fin de la r�servation de type ObjectProperty
  	 * @return la date de fin de la r�servation de type ObjectProperty
  	 */ 
	public ObjectProperty<Date> endProperty() {
		  return end;
	  }

	/**
  	 * La m�thode setEnd() permet de retourner la date de fin de la r�servation
  	 * @param end
  	 */
	public void setEnd(Date end) {
		  this.end.set(end);
	  }
}
